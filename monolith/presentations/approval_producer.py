import pika
import json


def approval_producer(presentation_object):
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="presentation_approvals")
    channel.basic_publish(
        exchange="",
        routing_key="presentation_approvals",
        body=json.dumps(
            {
                "presenter_name": presentation_object.presenter_name,
                "presenter_email": presentation_object.presenter_email,
                "title": presentation_object.title,
            }
        ),
    )
    connection.close()
