import pika
import json


def rejection_producer(presentation_object):
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="presentation_rejections")
    channel.basic_publish(
        exchange="",
        routing_key="presentation_rejections",
        body=json.dumps(
            {
                "presenter_name": presentation_object.presenter_name,
                "presenter_email": presentation_object.presenter_email,
                "title": presentation_object.title,
            }
        ),
    )
    connection.close()
