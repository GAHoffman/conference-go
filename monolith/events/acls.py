from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    headers = {"Authorization": PEXELS_API_KEY}
    # Create the URL for the request with the city and state
    params = {"per_page": 1, "query": city + " " + state}
    url = "https://api.pexels.com/v1/search?query="
    # Make the request
    response = requests.get(url, headers=headers, params=params)
    # Parse the JSON response
    content = json.loads(response.content)
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}

    # headers = {"Authorization": PEXELS_API_KEY}
    # params = {
    #     "per_page": 1,  # the default on the pexels page is 15, so here I set it to 1.
    #     "query": city + " " + state,
    # }
    # url = "https://api.pexels.com/v1/search"
    # response = requests.get(url, headers=headers, params=params)
    # content = json.loads(response.content)
    # try:
    #     return {"picture_url": content["photos"][0]["src"]["original"]}
    # except:
    #     return {"picture_url": None}


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    us_iso_code = "US"
    # More professional
    params = {
        "q": f"{city},{state},{us_iso_code}",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    geo_url = "http://api.openweathermap.org/geo/1.0/direct"
    # Make the request
    response_geo = requests.get(geo_url, params=params)
    # Parse the JSON response
    geocode = json.loads(response_geo.content)
    # Get the latitude and longitude from the response
    try:
        lat = geocode[0]["lat"]
        lon = geocode[0]["lon"]
    except:
        lat = None
        lon = None
    # Create the URL for the current weather API with the latitude
    #   and longitude
    # ******Using f string as proof of concept and params above to show how it works
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    # Make the request
    response_weather = requests.get(weather_url)
    # Parse the JSON response
    weather_data = json.loads(response_weather.content)
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
    try:
        return {
            "temperature": weather_data["main"]["temp"],
            "description": weather_data["weather"][0]["description"],
        }
    except:
        return {
            "temperature": None,
            "description": None,
        }
