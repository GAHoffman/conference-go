from django.contrib import admin

from .models import Conference, Location, State


@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )


@admin.register(State)
class StateAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )


@admin.register(Conference)
class ConferenceAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )
