from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class DateTimeEncoder(JSONEncoder):
    def default(self, o):
        # if o is an instance of datetime
        if isinstance(o, datetime):
            #    return o.isoformat()
            return o.isoformat()
        # otherwise
        else:
            #    return super().default(o)
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DateTimeEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        #   if the object to decode is the same class as what's in the
        #   model property, then
        if isinstance(o, self.model):
            #     * create an empty dictionary that will hold the property names
            #       as keys and the property values as values
            d = {}
            # if o has the attribute get_api_url
            #    then add its return value to the dictionary
            #    with the key "href"
            if hasattr(o, "get_api_url"):
                d["href"] = o.get_api_url()
            #     * for each name in the properties list
            for property in self.properties:
                #         * get the value of that property from the model instance
                #           given just the property name
                value = getattr(o, property)
                # update to look for specific encoders
                if property in self.encoders:
                    # pull from the encoder dictionary the encoder to use
                    encoder = self.encoders[property]
                    # set value = default value of the encoder running
                    value = encoder.default(value)
                #         * put it into the dictionary with that property name as
                #           the key
                d[property] = value
            # add reference to get_extra_data method created in this class below
            d.update(self.get_extra_data(o))
            #     * return the dictionary
            return d
        #   otherwise,
        #       return super().default(o)  # From the documentation
        else:
            return super().default(o)

    # add generic method in order to have placeholder above
    # to update dictionary with key-value pair
    def get_extra_data(self, o):
        return {}
